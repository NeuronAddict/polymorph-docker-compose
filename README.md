# Docker polymorph / web stack

This docker stack is composed by :
- a kali container with polymorph installed
- a web server (bob) with a simple php page
- a client (alice), who call the address http://bob/ every 20 seconds

## Run the stack :

```bash
$ git clone https://github.com/NeuronAddict/polymorph-docker-compose.git
$ cd polymorph-docker-compose
$ docker-compose up
```
